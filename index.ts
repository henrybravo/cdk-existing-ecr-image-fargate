import ecs = require('@aws-cdk/aws-ecs');
import cdk = require('@aws-cdk/core');
import ec2 = require('@aws-cdk/aws-ec2');
import * as iam from '@aws-cdk/aws-iam';

const envEU = { account: '123456789012', region: 'eu-west-1' };

class ExistingEcrImageFargate extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // create IAM Role for the Fargate ECS Task
    const ecsFargateServiceRole = new iam.Role(this, 'FargateTaskExecutionServiceRole', {
      assumedBy: new iam.ServicePrincipal('ecs-tasks.amazonaws.com')
    });

    // define the IAM policy to add to the IAM Role for the Fargate ECS Task
    ecsFargateServiceRole.addToPolicy(
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        resources: ['*'],
        actions: [            
          'ecr:GetAuthorizationToken',
          'ecr:BatchCheckLayerAvailability',
          'ecr:GetDownloadUrlForLayer',
          'ecr:BatchGetImage',
          'logs:CreateLogStream',
          'logs:PutLogEvents'
        ]
      })
    );
    
    // create a vpc with 2 public and 2 private subnets using natgw
    const vpc = new ec2.Vpc(this, 'MyNewVPC', { maxAzs: 2 });
    new cdk.CfnOutput(this, "MyVpc", {value: vpc.vpcId });
    
    const cluster = new ecs.Cluster(this, 'Cluster', { vpc });

    // create a CloudWatch Logs group
    const logging = new ecs.AwsLogDriver({
      streamPrefix: "cdk-existing-ecr-image-fargate",
    });

    // create a task definition with CloudWatch Logs
    const taskDef = new ecs.FargateTaskDefinition(this, "MyTaskDefinition", {
      memoryLimitMiB: 2048,
      cpu: 512,
      executionRole: ecsFargateServiceRole,
    });
    
    // define the container image to use from ecr repo in account
    const containerDef = new ecs.ContainerDefinition(this, "MyContainerDefinition", {
      image: ecs.ContainerImage.fromRegistry("123456789012.dkr.ecr.eu-west-1.amazonaws.com/my-container-image"),
      taskDefinition: taskDef,
      logging,
    });

    // container port to access the app
    containerDef.addPortMappings({
      containerPort: 8080
    });

    // Instantiate ECS Service with just cluster and image
    new ecs.FargateService(this, "FargateService", {
      cluster,
      taskDefinition: taskDef,
      assignPublicIp: true // if disabled the fargate tasks will be placed in a private subnet
    });
  }
}

const app = new cdk.App();

new ExistingEcrImageFargate(app, 'cdkExistingEcrImage', { env: envEU });

app.synth();
