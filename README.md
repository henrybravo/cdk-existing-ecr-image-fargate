# cdk-existing-ecr-image-fargate

cdk fargate app - deploys a fargate service without lb with existing ecr image from your account

## cdk version

tested with cdk version 1.70.0 (build c145314)

## deploy

Change the account number and the container image from ecr you need to deploy.

This branch deploys:

- IAM Role for the Fargate ECS Task
- defines the IAM policy to add to the IAM Role for the Fargate ECS Task
- a vpc with 2 public and 2 private subnets using natgw
- a CloudWatch Logs group
- a task definition with CloudWatch Logs
- the container image to use from ecr repo in account
- container port to access the app
- assigns Public Ip to task and placed in public subnet

**Checkout the [npvpc branch](https://gitlab.com/henrybravo/cdk-existing-ecr-image-fargate/-/tree/novpc) with different combo's of this app.**